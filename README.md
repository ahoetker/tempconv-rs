# tempconv

CLI temperature converter I wrote to try learning Rust.

## Usage

`tempconv TEMPERATURE+UNITS`

or

`tempconv TEMPERATURE UNITS`

## Examples
```
>>> tempconv 273.15 K

+------------+--------------------+
| Celsius    | 0                  |
+------------+--------------------+
| Fahrenheit | 31.999999999999943 |
+------------+--------------------+
| Kelvin     | 273.15             |
+------------+--------------------+
| Rankine    | 491.66999999999996 |
+------------+--------------------+

>>> tempconv -459.67F

+------------+---------+
| Celsius    | -273.15 |
+------------+---------+
| Fahrenheit | -459.67 |
+------------+---------+
| Kelvin     | 0       |
+------------+---------+
| Rankine    | 0       |
+------------+---------+
```

## Installation

I'm not distributing this to anybody because it's not useful. If you want to
install it, clone this repo and then use Cargo.

## Requirements

This package requires [prettytable-rs](https://github.com/phsym/prettytable-rs),
which will be installed automatically from the cargo.toml.
