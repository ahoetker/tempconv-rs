#[macro_use]
extern crate prettytable;
use prettytable::Table;
use std::env;

/// Returns a (QUANTITY, UNITS) tuple from CLI arguments.
fn parse_temp(args: Vec<String>) -> (f64, char) {
    let negator = match args[1].chars().next().unwrap() {
        'n' => -1.0,
        _ => 1.0,
    };

    let temp_tuple = if args.len() == 2 {
        let input_units = args[1].chars().last().unwrap();
        let input_temp = str::replace(&args[1][..&args[1].len() - 1], "n", "");
        (input_temp.parse::<f64>().unwrap() * negator, input_units)
    } else if args.len() == 3 {
        let input_temp = str::replace(&args[1], "n", "");
        (
            input_temp.parse::<f64>().unwrap() * negator,
            args[2].chars().last().unwrap(),
        )
    } else {
        panic!("USAGE: tempconv [TEMP?UNIT] ?[UNIT]")
    };
    return temp_tuple;
}

/// Returns a vector of (QUANTITY, UNITS) tuples from one such tuple.
/// Also checks that the given temp is above absolute zero.
fn conversions(given_temp: (f64, char)) -> Vec<(f64, String)> {
    let quantity = given_temp.0;
    let unit_c = String::from("Celsius");
    let unit_f = String::from("Fahrenheit");
    let unit_k = String::from("Kelvin");
    let unit_r = String::from("Rankine");
    match given_temp.1 {
        'C' => {
            assert!(quantity >= -273.15, "Non-physical temperature.");
            vec![
                (quantity, unit_c),
                (quantity * 1.8 + 32.0, unit_f),
                (quantity + 273.15, unit_k),
                ((quantity + 273.15) * 1.8, unit_r),
            ]
        }
        'F' => {
            assert!(quantity >= -459.67, "Non-physical temperature.");
            vec![
                ((quantity - 32.0) / 1.8, unit_c),
                (quantity, unit_f),
                ((quantity + 459.67) / 1.8, unit_k),
                (quantity + 459.67, unit_r),
            ]
        }
        'K' => {
            assert!(quantity >= 0.0, "Non-physical temperature.");
            vec![
                (quantity - 273.15, unit_c),
                (quantity * 1.8 - 459.67, unit_f),
                (quantity, unit_k),
                (quantity * 1.8, unit_r),
            ]
        }
        'R' => {
            assert!(quantity >= 0.0, "Non-physical temperature.");
            vec![
                (quantity / 1.8 - 273.15, unit_c),
                (quantity - 459.67, unit_f),
                (quantity / 1.8, unit_k),
                (quantity, unit_r),
            ]
        }
        _ => panic!("Bad units for temperature!"),
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let temperature = parse_temp(args);
    let converted_temps = conversions(temperature);
    let mut table = Table::new();
    for entry in converted_temps {
        table.add_row(row![entry.1, entry.0]);
    }
    table.printstd();
}
